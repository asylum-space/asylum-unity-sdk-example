using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public RectTransform rectTransform;

    // Update is called once per frame
    void Update()
    {
        rectTransform.Rotate(new Vector3(0, 0, 1));
    }
}
