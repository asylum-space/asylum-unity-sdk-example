using Asylum;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemView : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private TextMeshProUGUI _itemNameText;
    [SerializeField] private TextMeshProUGUI _itemDescriptionText;

    private AsylumItem _item;

    public void InitItem(AsylumItemAsyncHandler itemHandler)
    {
        _item = itemHandler.AsylumItem;

        _itemNameText.text = itemHandler.Name;
        _itemDescriptionText.text = itemHandler.Description;

        var spriteObj = itemHandler.GetAdapted(typeof(Sprite));

        if(spriteObj[0] == null
            || spriteObj[0] is not Sprite sprite)
        {
            UnityEngine.Debug.LogError($"Adapted sprite is null nor sprite type in the {itemHandler.Name} ItemView");
        }
        else
        {
            //Mb we can set null sprite to the image?
            _image.sprite = sprite;
        }
    }
}
