using Asylum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsController : MonoBehaviour
{
    [SerializeField] private AsylumDigitalObjectsController _asylumDigitalObjectsControllerInstance;
    
    [Space]
    [SerializeField] private GameObject _itemViewPrefab;
    [SerializeField] private Transform _content;

    void Start()
    {
        var items = _asylumDigitalObjectsControllerInstance.ItemHandlers;

        if(items != null && items.Count > 0)
        {
            items.ForEach(item => OnItemLoaded(item));
        }
        else
        {
            _asylumDigitalObjectsControllerInstance.OnItemWasLoaded += OnItemLoaded;
        }
    }

    private void OnItemLoaded(AsylumItemAsyncHandler itemHandler)
    {
        var itemView = Instantiate(_itemViewPrefab, _content).GetComponent<ItemView>();

        if(itemView != null)
        {
            itemView.InitItem(itemHandler);
        }
        else
        {
            Destroy(itemView.gameObject);
        }
    }    
}
