## Installation process:

1. Download Unity (supports version **2021.3.1f1**)
2.  Clone this repository with [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
    
    `$ git clone --recurse-submodules https://gitlab.com/asylum-space/asylum-unity-sdk-example`

3.  Resolve dependencies :
    - Import [GLTF plugin](https://github.com/Siccity/GLTFUtility) if you want to add and test functionality for the 3D model implementation
        > Note: You need to follow [these steps](https://github.com/Siccity/GLTFUtility/pull/203) to use GLTFUtility in the WebGL build
        
        **or**
    
    - Delete _"\AsylumSDK\ScriptableObjects Instances\GLB3DModelTag.asset"_ and _"AsylumSDK\Tags\Implemented\GLB3DModelTag.cs"_ from the SDK folder for removing non-using dependency for 3D models implementations
4. You can test plugin in the [Play Mode](https://gitlab.com/asylum-space/asylum-unity-sdk/-/blob/main/Docs/PlayMode.md)
5. When you're ready to build, select one of the options:
    - [Run Unity WebGL build inside Creator Studio](https://gitlab.com/asylum-space/asylum-unity-sdk/-/blob/main/Docs/WEBGL.md)
    - [Run Unity Standalone build for Windows, Mac, Linux](https://gitlab.com/asylum-space/asylum-unity-sdk/-/blob/main/Docs/StandaloneWindowsBuild.md)

# Test and code coverage
Code coverage report is already added into the project! You can see it by [CodeCoverage/Report](https://gitlab.com/asylum-space/asylum-unity-sdk-example/-/tree/main/CodeCoverage/Report)/index.html

If you want to run tests by yourself and|or in the different project you should follow these steps:
- Install [Unity Test Framework](https://docs.unity3d.com/Packages/com.unity.test-framework@1.1/manual/index.html) and open its window after by **Window/General/Test Runner** in the Unity Engine
- Install [Unity Code Coverage asset](https://docs.unity3d.com/Packages/com.unity.testtools.codecoverage@1.1/manual/index.html) open its window after by **Window/Analysis/Code Coverage** in the Unity Engine
    > Note: it is **necessary** to run the node with ipfs server and complete seed as well for completing all the tests
